/*
 * Copyright (c) 2011 Yahoo! Inc.
 *
 * Yahoo! Inc. licenses this file to you under the Apache License, Version
 * 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nodus.js;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

import nodus.http.Http;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.ContextAction;
import org.mozilla.javascript.ContextFactory;
import org.mozilla.javascript.Scriptable;
import org.mozilla.javascript.ScriptableObject;
import org.mozilla.javascript.commonjs.module.ModuleScriptProvider;
import org.mozilla.javascript.commonjs.module.Require;
import org.mozilla.javascript.commonjs.module.provider.SoftCachingModuleScriptProvider;
import org.mozilla.javascript.commonjs.module.provider.UrlModuleSourceProvider;

/**
 * @author Bruno Fernandez-Ruiz
 */
public class Driver {
    private static final String NODUS = "nodus.js";

    public static void exec(final List<URI> paths, final URI mainModule) throws URISyntaxException {
        if (paths == null || paths.size() == 0) {
            throw new IllegalArgumentException("no require paths specified");
        }
        ContextFactory ctxFactory = ContextFactory.getGlobal();
        if (ctxFactory == null) {
            ctxFactory = new ContextFactory();
            ContextFactory.initGlobal(ctxFactory);
        }
        ctxFactory.call(new ContextAction() {
            @Override
            public Object run(Context ctx) {
                // The top-level shared and sealed scope, there should be only one such instance
                ScriptableObject scope = ctx.initStandardObjects(null, true);
                // -1=do not compile; 9=compile
                // TODO: force compilation once Rhino bug is fixed https://bugzilla.mozilla.org/show_bug.cgi?id=630111
                ctx.setOptimizationLevel(-1);
                String resource = Thread.currentThread().getContextClassLoader().getResource(NODUS).toExternalForm();
                URI directory;
                try {
                    directory = new URI(resource.substring(0, resource.lastIndexOf('/') + 1));
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
                ModuleScriptProvider provider = new SoftCachingModuleScriptProvider(
                        new UrlModuleSourceProvider(Collections.singleton(directory), paths));
                Require require = new Require(ctx, scope, provider, null, null, false);
                ScriptableObject.putProperty(scope, "require", require);
                Scriptable global = require.requireMain(ctx, "nodus");
                ScriptableObject.putProperty(scope, "global", global);
                ScriptableObject.putProperty(scope, "console", global.get("console", global));

                try {
                    Http.setThreadFactory(new RhinoThreadFactory());
                    InputStreamReader is = new InputStreamReader(mainModule.toURL().openStream());
                    Object obj = ctx.evaluateReader(scope, is, mainModule.toString(), 1, null);
                    // we are done populating the top-level scope, let's seal it
                    scope.sealObject();
                    return obj;
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        });
    }

    /**
     * Code adapted from Executors.DefaultThreadFactory.
     */
    private static class RhinoThreadFactory implements ThreadFactory {
        static final AtomicInteger poolNumber = new AtomicInteger(1);
        final ThreadGroup group;
        final AtomicInteger threadNumber = new AtomicInteger(1);
        final String namePrefix;

        RhinoThreadFactory() {
            SecurityManager s = System.getSecurityManager();
            group = (s != null) ? s.getThreadGroup() : Thread.currentThread().getThreadGroup();
            namePrefix = "pool-" + poolNumber.getAndIncrement() + "-thread-";
        }

        public Thread newThread(final Runnable r) {

            Runnable proxy = new Runnable() {
                @Override
                public void run() {
                    ContextFactory.getGlobal().call(new ContextAction() {
                        @Override
                        public Object run(Context ctx) {
                            r.run();
                            return null;
                        }
                    });
                }
            };
            Thread t = new Thread(group, proxy, namePrefix + threadNumber.getAndIncrement(), 0);
            if (t.isDaemon()) {
                t.setDaemon(false);
            }
            if (t.getPriority() != Thread.NORM_PRIORITY) {
                t.setPriority(Thread.NORM_PRIORITY);
            }
            return t;
        }
    }
}
