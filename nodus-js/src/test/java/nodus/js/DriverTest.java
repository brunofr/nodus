/*
 * Copyright (c) 2011 Yahoo! Inc.
 *
 * Yahoo! Inc. licenses this file to you under the Apache License, Version
 * 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nodus.js;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;

import org.junit.Test;

import static org.junit.Assert.fail;

/**
 * @author Bruno Fernandez-Ruiz
 */
public class DriverTest {
    private String getResource(String name) {
        URL resource = Thread.currentThread().getContextClassLoader().getResource(name);
        if (resource != null) {
            return resource.toExternalForm();
        } else {
            return null;
        }
    }

    private URI getDirectory(String resource) {
        if (resource == null) {
            return null;
        }
        try {
            return new URI(resource.substring(0, resource.lastIndexOf('/') + 1));
        } catch (URISyntaxException e) {
            return null;
        }
    }

    @Test
    public void testGlobalObject() {
        String resource = getResource("test1.js");
        try {
            Driver.exec(Arrays.asList(getDirectory(resource)), new URI(resource));
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testRequireNative() {
        String resource = getResource("test2.js");
        try {
            Driver.exec(Arrays.asList(getDirectory(resource)), new URI(resource));
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testRequireLocal() {
        String resource = getResource("test3.js");
        try {
            Driver.exec(Arrays.asList(getDirectory(resource)), new URI(resource));
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testHttp() {
        String resource = getResource("httpTest1.js");
        try {
            Driver.exec(Arrays.asList(getDirectory(resource)), new URI(resource));
        } catch (Exception e) {
            fail();
        }
    }
}
