/*
 * Copyright (c) 2011 Bruno Fernandez-Ruiz.
 *
 * Bruno Fernandez-Ruiz licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nodus.js;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;
import org.mozilla.javascript.Context;
import org.mozilla.javascript.ContextAction;
import org.mozilla.javascript.ContextFactory;
import org.mozilla.javascript.ScriptableObject;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Rhino generated classes cannot be loaded in JDk7 as they are being assigned
 * major.minor 0.0 version: https://bugzilla.mozilla.org/show_bug.cgi?id=630111
 *
 * @author Bruno Fernandez-Ruiz
 */
public class RhinoBugTest {
    @Test
    public void testJdk7Compilation() {
        ContextFactory contextFactory = ContextFactory.getGlobal();
        contextFactory.call(new ContextAction() {
            @Override
            public Object run(Context ctx) {
                ScriptableObject scope = ctx.initStandardObjects(null, true);
                ctx.setOptimizationLevel(9);
                return ctx.evaluateString(scope, "var h = 'test'", "test", 0, null);
            }
        });
    }

    @Test
    public void testJdk7MajorMinor() throws IOException {
        String thisClass = "nodus/js/RhinoBugTest.class";
        InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(thisClass);
        DataInputStream dis = new DataInputStream(is);
        assertNotNull(is);
        dis.readInt(); // magic header
        int minor = dis.readUnsignedShort();
        int major = dis.readUnsignedShort();
        assertTrue(major > 0);
        assertTrue(minor >= 0);
    }
}
