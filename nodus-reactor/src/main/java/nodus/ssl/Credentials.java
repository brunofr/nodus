/*
 * Copyright (c) 2011 Yahoo! Inc.
 *
 * Yahoo! Inc. licenses this file to you under the Apache License, Version
 * 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nodus.ssl;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;

/**
 * Reads a private key (DER PKCS8 format) and a certificate (X.509) to create a set of
 * of credentials used to generate SSLContext.
 * <p/>
 * To generate a self-certificate with openssl
 * (see <a href="http://httpd.apache.org/docs/2.2/ssl/ssl_faq.html#selfcert">Apache HTTP 2.2 FAQ</a>
 * <p/>
 * <pre>
 * <code>
 * $ openssl req -new -x509 -nodes -out server.crt -keyout server.pem
 * $ openssl pkcs8 -topk8 -inform PEM -outform DER -in server.pem -out server.der -nocrypt
 * </code>
 * </pre>
 *
 * @author Bruno Fernandez-Ruiz
 */
public class Credentials {
    private static final String PROTOCOL = "TLS";
    private SSLContext sslContext;

    public Credentials(File privateKey, File certificate) {
        if (privateKey == null || !privateKey.exists()) {
            throw new IllegalArgumentException("Private key does not exist: " + privateKey);
        }
        if (certificate == null || !certificate.exists()) {
            throw new IllegalArgumentException("Certificate does not exist: " + certificate);
        }
        try {
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            FileInputStream fis = new FileInputStream(certificate);
            Certificate[] cert = new Certificate[1];
            cert[0] = cf.generateCertificate(fis);

            fis = new FileInputStream(privateKey);
            DataInputStream dis = new DataInputStream(fis);
            byte[] keyBytes = new byte[(int) privateKey.length()];
            dis.readFully(keyBytes);
            dis.close();
            PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);
            PrivateKey key = KeyFactory.getInstance("RSA").generatePrivate(spec);

            // create an empty keystore
            KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
            ks.load(null, null);
            ks.setKeyEntry("localhost", key, new char[0], cert);

            // Set up key manager factory to use our key store
            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(ks, new char[0]);

            // Initialize the SSLContext to work with our key managers.
            sslContext = SSLContext.getInstance(PROTOCOL);
            sslContext.init(kmf.getKeyManagers(), null, null);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("File not found", e);
        } catch (CertificateException e) {
            throw new IllegalArgumentException("Invalid certificate", e);
        } catch (UnrecoverableKeyException e) {
            throw new IllegalArgumentException("Invalid key", e);
        } catch (NoSuchAlgorithmException e) {
            throw new Error("Missing algorithm implementation", e);
        } catch (KeyStoreException e) {
            throw new IllegalArgumentException("Invalid key", e);
        } catch (IOException e) {
            throw new IllegalArgumentException("Cannot read file", e);
        } catch (KeyManagementException e) {
            throw new Error("Unknown error", e);
        } catch (InvalidKeySpecException e) {
            throw new IllegalArgumentException("Invalid key", e);
        }
    }

    public SSLContext getSSLContext() {
        return sslContext;
    }
}
