/*
 * Copyright (c) 2011 Yahoo! Inc.
 *
 * Yahoo! Inc. licenses this file to you under the Apache License, Version
 * 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nodus.examples;

import java.util.HashMap;

import nodus.http.Http;
import nodus.http.server.RequestListener;
import nodus.http.server.Server;
import nodus.http.server.ServerRequest;
import nodus.http.server.ServerResponse;
import nodus.http.websocket.ConnectionListener;
import nodus.http.websocket.SocketListener;
import nodus.http.websocket.SocketServer;
import nodus.http.websocket.WebSocket;

/**
 * @author Bruno Fernandez-Ruiz
 */
public class EchoWebSocketServer {
    public static void main(String[] args) {
        Server server = Http.createServer();
        server.setRequestListener(new RequestListener() {
            @Override
            public void service(ServerRequest request, ServerResponse response) {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "text/html; charset=utf-8");
                headers.put("Content-Length", "47");
                response.writeHead(200, headers);
                response.end("<html><body><h1>hüllo world</h1></body></html>");
            }
        });
        server.listen(8080);

        SocketServer wsServer = new SocketServer();
        wsServer.listen(server, new ConnectionListener() {
            @Override
            public void onConnect(final WebSocket socket) {
                socket.setSocketListener(new SocketListener() {
                    @Override
                    public void onMessage(String message) {
                        System.out.println(message);
                        socket.send(message);
                    }

                    @Override
                    public void onClose() {
                    }

                    @Override
                    public void onError(Throwable cause) {
                    }
                });
            }
        });
    }
}
