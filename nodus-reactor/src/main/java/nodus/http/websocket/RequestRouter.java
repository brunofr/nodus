/*
 * Copyright (c) 2011 Yahoo! Inc.
 *
 * Yahoo! Inc. licenses this file to you under the Apache License, Version
 * 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nodus.http.websocket;

import java.util.HashMap;
import java.util.Map;

import nodus.http.server.RequestListener;
import nodus.http.server.ServerRequest;
import nodus.http.server.ServerResponse;
import nodus.inject.Injector;


/**
 * @author Bruno Fernandez-Ruiz
 */
class RequestRouter implements RequestListener {
    private RequestListener requestListener;
    private ConnectionListener connectionListener;

    public RequestRouter(RequestListener requestListener, ConnectionListener connectionListener) {
        this.requestListener = requestListener;
        this.connectionListener = connectionListener;
    }

    @Override
    public void service(ServerRequest request, ServerResponse response) {
        Map<String, String> headers = request.getHeaders();
        boolean isWebSocket = "GET".equals(request.getMethod()) &&
                "Upgrade".equals(headers.get("Connection")) &&
                "WebSocket".equals(headers.get("Upgrade"));
        if (isWebSocket) {
            HandshakeListener hsListener = Injector.getInstance(HandshakeListener.class);
            hsListener.setConnectionListener(connectionListener);
            hsListener.service(request, response);
        } else {
            if (requestListener != null) {
                requestListener.service(request, response);
            } else {
                response.writeHead(404, new HashMap<String, String>().put("Content-Type", "text/plain"));
                response.end("Resource not found");
            }
        }
    }
}
