/*
 * Copyright (c) 2011 Yahoo! Inc.
 *
 * Yahoo! Inc. licenses this file to you under the Apache License, Version
 * 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nodus.http.websocket.netty.codec;

import javax.naming.OperationNotSupportedException;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.frame.TooLongFrameException;
import org.jboss.netty.handler.codec.replay.ReplayingDecoder;
import org.jboss.netty.handler.codec.replay.VoidEnum;
import org.jboss.netty.util.CharsetUtil;

/**
 * Decodes a WebSocket frame per the draft-00 spec. Limits frame size
 * to 16385 bytes to avoid buffer overflow attacks.
 *
 * @author Bruno Fernandez-Ruiz
 */
public class WebSocketFrameDecoder extends ReplayingDecoder<VoidEnum> {
    private static final int MAX_FRAME_BUFFER_SIZE = 16384;

    @Override
    protected Object decode(ChannelHandlerContext ctx, Channel channel,
                            ChannelBuffer buffer, VoidEnum state) throws Exception {
        byte type = buffer.readByte();

        if (((type >>> 7) & 1) == 0) {
            // If the most significant bit of /type/ is not set
            if (type != 0) {
                // If /type/ is not a 0x00 byte, then the server may abort these
                // steps and either immediately disconnect from the client or
                // set the /client terminated/ flag.
                ctx.getChannel().close();
                return null;
            } else {
                // Read from the stream until a 0xFF byte is found. Treat read bytes
                // as a UTF-8 string.
                int current = buffer.readerIndex();
                int limit = buffer.indexOf(current, current + buffer.readableBytes(), (byte) 0xFF);
                if (limit == -1) {
                    // there is no enough data yet, wait until we get a 0xFF byte
                    if (buffer.readableBytes() > MAX_FRAME_BUFFER_SIZE) {
                        // even if there is no 0XFF yet, the frame is already too big, drop data
                        throw new TooLongFrameException();
                    }
                    return null;
                }
                int payloadLength = limit - current;
                if (payloadLength > MAX_FRAME_BUFFER_SIZE) {
                    throw new TooLongFrameException();
                }
                ChannelBuffer bytes = buffer.readBytes(limit - current);
                buffer.readByte(); // 0xFF
                return new DefaultWebSocketFrame(bytes.toString(CharsetUtil.UTF_8));
            }
        } else {
            throw new OperationNotSupportedException("Decoding binary frames is not supported.");
        }
    }

}
