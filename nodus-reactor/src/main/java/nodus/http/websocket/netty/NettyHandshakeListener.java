/*
 * Copyright (c) 2011 Yahoo! Inc.
 *
 * Yahoo! Inc. licenses this file to you under the Apache License, Version
 * 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nodus.http.websocket.netty;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import nodus.http.server.ServerRequest;
import nodus.http.server.ServerResponse;
import nodus.http.server.netty.NettyServerRequest;
import nodus.http.websocket.ConnectionListener;
import nodus.http.websocket.HandshakeListener;
import nodus.http.websocket.ReadyState;
import nodus.http.websocket.netty.codec.WebSocketFrameDecoder;
import nodus.http.websocket.netty.codec.WebSocketFrameEncoder;
import org.jboss.netty.channel.ChannelPipeline;

/**
 * @author Bruno Fernandez-Ruiz
 */
public class NettyHandshakeListener implements HandshakeListener {
    private static final MessageDigest MD5;

    static {
        try {
            MD5 = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            throw new Error("MD5 algorithm missing in JVM!");
        }
    }

    private ConnectionListener connectionListener;

    @Override
    public void setConnectionListener(ConnectionListener connectionListener) {
        this.connectionListener = connectionListener;
    }

    @Override
    public void service(ServerRequest request, ServerResponse response) {
        Map<String, String> requestHeaders = request.getHeaders();
        String host = requestHeaders.get("Host");
        String origin = requestHeaders.get("Origin");
        if (origin == null) {
            response.writeHead(400, new HashMap<String, String>().put("Content-Type", "text/plain"));
            response.end("Bad request");
            return;
        }

        // TODO Sec-WebSocket-Protocol header seems to be always null
        // TODO Sec-WebSocket-Draft header seems to be always null

        String wsKey1 = requestHeaders.get("Sec-WebSocket-Key1");
        long number1 = Long.parseLong(wsKey1.replaceAll("[^0-9]", ""));
        long spaces1 = wsKey1.replaceAll("[^ ]", "").length();

        String wsKey2 = requestHeaders.get("Sec-WebSocket-Key2");
        long number2 = Long.parseLong(wsKey2.replaceAll("[^0-9]", ""));
        long spaces2 = wsKey2.replaceAll("[^ ]", "").length();

        // If either /spaces_1/ or /spaces_2/ is zero, then abort the
        // WebSocket connection.  This is a symptom of a cross-protocol
        // attack.
        // If /key-number_1/ is not an integral multiple of /spaces_1/,
        // then abort the WebSocket connection.
        if (spaces1 == 0 || spaces2 == 0 || number1 % spaces1 != 0 || number2 % spaces2 != 0) {
            response.writeHead(400, new HashMap<String, String>().put("Content-Type", "text/plain"));
            response.end("Bad request");
            return;
        }

        long key1 = number1 / spaces1;
        long key2 = number2 / spaces2;
        long key3 = request.getContent().getLong();

        // Let /challenge/ be the concatenation of /part_1/, expressed as a
        // big-endian 32 bit integer, /part_2/, expressed as a big-endian
        // 32 bit integer, and the eight bytes of /key_3/ in the order they
        // were sent on the wire.

        ByteBuffer challenge = ByteBuffer.allocate(16);
        challenge.putInt((int) key1);
        challenge.putInt((int) key2);
        challenge.putLong(key3);

        HashMap<String, String> responseHeaders = new HashMap<String, String>();
        responseHeaders.put("Upgrade", "WebSocket");
        responseHeaders.put("Connection", "Upgrade");
        String scheme = request.isHttps() ? "wss" : "ws";
        // we playback our location
        String location = String.format("%s://%s%s", scheme, host, request.getUrl());
        responseHeaders.put("Sec-WebSocket-Location", location);
        responseHeaders.put("Sec-WebSocket-Origin", origin);
        String protocol = requestHeaders.get("Sec-WebSocket-Protocol");
        if (protocol != null) {
            responseHeaders.put("Sec-WebSocket-Protocol", protocol);
        }

        // We first write the upgrade response, and then change the encoder and decoder.
        // The WebSocket example from Netty first replaces the decoder, then writes, then replaces the encoder.
        // However, since the thread is already assigned to this channel, it won't handle any more client
        // writes until this method ends and goes back to the event loop. This gives us an opportunity to change
        // the pipeline.

        response.writeHead(101, "WebSocket Protocol Handshake", responseHeaders);
        // Let /response/ be the MD5 fingerprint of /challenge/ as a big-endian 128 bit string.
        response.end(ByteBuffer.wrap(MD5.digest(challenge.array())));

        // from here on, the code depends on netty
        NettyServerRequest nettyServerRequest = (NettyServerRequest) request;
        ChannelPipeline pipeline = nettyServerRequest.getContext().getPipeline();
        NettyWebSocket socket = new NettyWebSocket(pipeline.getChannel());

        pipeline.replace("decoder", "wsdecoder", new WebSocketFrameDecoder());
        pipeline.replace("encoder", "wsencoder", new WebSocketFrameEncoder());

        socket.setReadyState(ReadyState.OPEN);
        connectionListener.onConnect(socket);

    }
}
