/*
 * Copyright (c) 2011 Yahoo! Inc.
 *
 * Yahoo! Inc. licenses this file to you under the Apache License, Version
 * 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nodus.http.websocket.netty.codec;

import java.nio.ByteOrder;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;
import org.jboss.netty.util.CharsetUtil;

/**
 * @author Bruno Fernandez-Ruiz
 */
public class WebSocketFrameEncoder extends OneToOneEncoder {
    @Override
    protected Object encode(ChannelHandlerContext ctx, Channel channel, Object msg) throws Exception {
        if (msg instanceof WebSocketFrame) {
            String text = ((WebSocketFrame) msg).getText();
            ChannelBuffer encoded = ChannelBuffers.buffer(ByteOrder.BIG_ENDIAN, text.length() + 2);
            encoded.writeByte(0x00);
            encoded.writeBytes(text.getBytes(CharsetUtil.UTF_8));
            encoded.writeByte(0xFF);
            return encoded;
        } else {
            throw new UnsupportedOperationException("Encoding of binary frames not supported");
        }
    }
}
