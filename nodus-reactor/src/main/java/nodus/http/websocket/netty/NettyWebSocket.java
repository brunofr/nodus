/*
 * Copyright (c) 2011 Yahoo! Inc.
 *
 * Yahoo! Inc. licenses this file to you under the Apache License, Version
 * 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nodus.http.websocket.netty;

import nodus.http.websocket.ReadyState;
import nodus.http.websocket.SocketListener;
import nodus.http.websocket.WebSocket;
import nodus.http.websocket.netty.codec.DefaultWebSocketFrame;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelFutureListener;

/**
 * @author Bruno Fernandez-Ruiz
 */
public class NettyWebSocket implements WebSocket {
    private Channel channel;
    private SocketListener listener;
    private ReadyState readyState;

    public NettyWebSocket(Channel channel) {
        this.channel = channel;
        this.readyState = ReadyState.CONNECTING;
    }

    @Override
    public String getUrl() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ReadyState getReadyState() {
        return readyState;
    }

    @Override
    public String getProtocol() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public long getBufferedAmount() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setSocketListener(SocketListener listener) {
        this.listener = listener;
        channel.getPipeline().replace("handler", "wshandler", new WebSocketHandler(listener));
    }

    @Override
    public void send(String data) {
        channel.write(new DefaultWebSocketFrame(data));
    }

    @Override
    public void close() {
        readyState = ReadyState.CLOSING;
        channel.close().addListener(new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture future) throws Exception {
                readyState = ReadyState.CLOSED;
                if (listener != null) {
                    listener.onClose();
                }
            }
        });
    }

    public void setReadyState(ReadyState state) {
        readyState = state;
    }
}
