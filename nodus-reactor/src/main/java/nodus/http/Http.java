/*
 * Copyright (c) 2011 Yahoo! Inc.
 *
 * Yahoo! Inc. licenses this file to you under the Apache License, Version
 * 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nodus.http;

import java.util.concurrent.ThreadFactory;

import nodus.http.client.Client;
import nodus.http.server.Server;
import nodus.http.websocket.SocketServer;
import nodus.inject.Injector;

/**
 * @author Bruno Fernandez-Ruiz
 */
public class Http {
    private static ThreadFactory threadFactory;

    public static void setThreadFactory(ThreadFactory threadFactory) {
        Http.threadFactory = threadFactory;
    }
    public static Server createServer() {
        Server server = Injector.getInstance(Server.class);
        if (threadFactory != null) {
            server.setThreadFactory(threadFactory);
        }
        return server;
    }

    public static Client createClient() {
        return Injector.getInstance(Client.class);
    }

    public static SocketServer createSocketServer() {
        return Injector.getInstance(SocketServer.class);
    }
}
