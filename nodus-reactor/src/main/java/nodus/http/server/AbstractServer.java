/*
 * Copyright (c) 2011 Yahoo! Inc.
 *
 * Yahoo! Inc. licenses this file to you under the Apache License, Version
 * 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nodus.http.server;

import java.net.InetSocketAddress;

import nodus.ssl.Credentials;

/**
 * @author Bruno Fernandez-Ruiz
 */
public abstract class AbstractServer implements Server {
    protected Credentials credentials;
    protected RequestListener listener;

    @Override
    public void setSecure(Credentials credentials) {
        this.credentials = credentials;
    }

    @Override
    public void setRequestListener(RequestListener listener) {
        this.listener = listener;
    }

    @Override
    public RequestListener getRequestListener() {
        return listener;
    }

    @Override
    public void listen(int port, String host) {
        listen(getHttpAddress(host, port));
    }

    @Override
    public void listen(int port) {
        listen(port, null);
    }

    protected abstract void listen(InetSocketAddress httpAddress);

    private InetSocketAddress getHttpAddress(String host, int port) {
        InetSocketAddress address;

        if (port == 0) {
            if (credentials != null) {
                port = 443;
            } else {
                port = 80;
            }
        }

        if (host != null) {
            address = new InetSocketAddress(host, port);
        } else {
            // AnyLocal
            address = new InetSocketAddress(port);
        }
        return address;
    }
}
