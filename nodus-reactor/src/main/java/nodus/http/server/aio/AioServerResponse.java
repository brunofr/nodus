/*
 * Copyright (c) 2011 Yahoo! Inc.
 *
 * Yahoo! Inc. licenses this file to you under the Apache License, Version
 * 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nodus.http.server.aio;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Map;

import nodus.http.server.ServerResponse;

/**
 * @author Bruno Fernandez-Ruiz
 */
public class AioServerResponse implements ServerResponse {
    @Override
    public void writeHead(int statusCode, String reasonPhrase, Map<String, String> headers) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeHead(int statusCode) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeHead(int statusCode, String reasonPhrase) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void writeHead(int statusCode, Map<String, String> headers) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void write(String chunk) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void write(String chunk, Charset charset) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void write(ByteBuffer binaryChunk) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void end() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void end(String chunk) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void end(String chunk, Charset charset) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void end(ByteBuffer binaryChunk) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
