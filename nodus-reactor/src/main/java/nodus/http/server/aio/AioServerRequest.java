/*
 * Copyright (c) 2011 Yahoo! Inc.
 *
 * Yahoo! Inc. licenses this file to you under the Apache License, Version
 * 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nodus.http.server.aio;

import java.nio.ByteBuffer;
import java.util.Map;

import nodus.http.server.ServerRequest;

/**
 * @author Bruno Fernandez-Ruiz
 */
public class AioServerRequest implements ServerRequest {
    @Override
    public boolean isHttps() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getMethod() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getUrl() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Map<String, String> getHeaders() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Map<String, String> getTrailers() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public String getHttpVersion() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public ByteBuffer getContent() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setEventListener(EventListener listener) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
