/*
 * Copyright (c) 2011 Yahoo! Inc.
 *
 * Yahoo! Inc. licenses this file to you under the Apache License, Version
 * 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nodus.http.server.aio;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;

import nodus.http.server.RequestListener;

/**
* @author Bruno Fernandez-Ruiz
*/
class AioRequestHandler implements CompletionHandler<AsynchronousSocketChannel, Void> {
    private static final ByteBuffer response = ByteBuffer.wrap(("HTTP/1.1 200 OK\n"
            + "Content-Type: text/html; charset=utf-8\n"
            + "Content-Length: 20\n\n"
            + "<h1>HELLO WORLD</h1>").getBytes());

    private AsynchronousServerSocketChannel server;
    private RequestListener listener;
    private ByteBuffer readBuffer = ByteBuffer.allocate(1024);

    public AioRequestHandler(AsynchronousServerSocketChannel server, RequestListener listener) {
        this.server = server;
        this.listener = listener;
    }

    @Override
    public void failed(Throwable exc, Void attachment) {
        exc.printStackTrace();
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void completed(final AsynchronousSocketChannel channel, Void attachment) {
        server.accept(null, new AioRequestHandler(server, listener));
        readBuffer.clear();
        channel.read(readBuffer, null, new CompletionHandler<Integer, Object>() {
            @Override
            public void completed(Integer result, Object attachment) {
                response.rewind();
                listener.service(new AioServerRequest(), new AioServerResponse());
                channel.write(response, null, new CompletionHandler<Integer, Object>() {
                    @Override
                    public void completed(Integer result, Object attachment) {
                        try {
                            channel.close();
                        } catch (IOException e) {
                            System.out.println(e.toString());
                        }
                    }

                    @Override
                    public void failed(Throwable exc, Object attachment) {
                        exc.printStackTrace();
                        throw new UnsupportedOperationException("Not supported yet.");
                    }
                });
            }

            @Override
            public void failed(Throwable exc, Object attachment) {
                exc.printStackTrace();
                throw new UnsupportedOperationException("Not supported yet.");
            }
        });
    }

    ;

}
