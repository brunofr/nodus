/*
 * Copyright (c) 2011 Yahoo! Inc.
 *
 * Yahoo! Inc. licenses this file to you under the Apache License, Version
 * 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nodus.http.server.aio;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.AsynchronousServerSocketChannel;
import java.util.concurrent.ThreadFactory;

import nodus.http.server.AbstractServer;

/**
 *
 * @author Bruno Fernandez-Ruiz
 */
public class AioServer extends AbstractServer {
    private AsynchronousServerSocketChannel server;

    @Override
    public void setThreadFactory(ThreadFactory threadFactory) {
        // TODO use provided thread factory instead of default kernel thread group
    }

    @Override
    protected void listen(InetSocketAddress httpAddress) {
        try {
            server = AsynchronousServerSocketChannel.open();
            server.bind(httpAddress);
            server.accept(null,  new AioRequestHandler(server, getRequestListener()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        try {
            server.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}