/*
 * Copyright (c) 2011 Yahoo! Inc.
 *
 * Yahoo! Inc. licenses this file to you under the Apache License, Version
 * 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nodus.http.server;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Map;

/**
 * A handle on the Http response object that allows writing both fixed-length and
 * chunked messages.
 * <p/>
 * If a 'Transfer-Encoding: chunked' header is found in the headers structure, the
 * response will be chunked, and every consequent call to write() or end() will
 * result in a separate chunk. Otherwise, the response will be buffered until
 * a call to end() is made to flush and end the response. Except for responses with
 * a status code of 10x, if no Content-Length header is found, the
 * 'Transfer-Encoding: chunked' header will be added automatically.
 *
 * @author Bruno Fernandez-Ruiz
 */
public interface ServerResponse {
    /**
     * Writes the header response, with the provided status code and response phrase.
     * The provided headers are written. After this call only the message body or
     * trailers can be written.
     * <p/>
     *
     * @param statusCode   a 100 < integer < 999
     * @param reasonPhrase a human readable phrase
     * @param headers      the headers to output. multi-value headers should have values
     *                     separated by commas (',')
     */
    void writeHead(int statusCode, String reasonPhrase, Map<String, String> headers);

    /**
     * Writes the header response, with the provided status code, and the default
     * response phrase associated with that status code.
     *
     * @param statusCode
     * @see #writeHead(int, String, java.util.Map)
     */
    void writeHead(int statusCode);

    /**
     * Writes the header response, with the provided status code and response phrase.
     * No other header is emitted.
     *
     * @param statusCode
     * @param reasonPhrase
     * @see #writeHead(int, String, java.util.Map)
     */
    void writeHead(int statusCode, String reasonPhrase);

    /**
     * Write the header response, with the provided status code, the default reason phrase
     * associated with that status code, and the provided headers.
     *
     * @param statusCode
     * @param headers
     * @see #writeHead(int, String, java.util.Map)
     */
    void writeHead(int statusCode, Map<String, String> headers);

    /**
     * Writes the provided chunk onto the response buffer as a UTF-8 string.
     *
     * @param chunk
     * @see #write(java.nio.ByteBuffer)
     */
    void write(String chunk);

    /**
     * Writes the provided chunk onto the response buffer with the provided charset encoding.
     *
     * @param chunk
     * @param charset
     * @see #write(java.nio.ByteBuffer)
     */
    void write(String chunk, Charset charset);

    /**
     * Writes the provided buffer onto the response buffer. If the message is chunked, this
     * results in a chunk sent on the wire, otherwise it's buffered until a call to end()
     * is made.
     *
     * @param binaryChunk
     */
    void write(ByteBuffer binaryChunk);

    /**
     * Ends the response stream. If the message is chunked, it sends a final line (\r\n),
     * otherwise it emits the buffered response.
     */
    void end();

    /**
     * Writes the provided chunk and ends the response stream.
     *
     * @param chunk
     * @see #write(String)
     * @see #end()
     */
    void end(String chunk);

    /**
     * Writes the provided chunk using the provided charset encoding an ends the response.
     *
     * @param chunk
     * @param charset
     * @see #write(String, java.nio.charset.Charset)
     * @see #end()
     */
    void end(String chunk, Charset charset);

    /**
     * Writes the provided chunk and ends the response.
     *
     * @param binaryChunk
     * @see #write(java.nio.ByteBuffer)
     * @see #end()
     */
    void end(ByteBuffer binaryChunk);
}
