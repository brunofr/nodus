/*
 * Copyright (c) 2011 Yahoo! Inc.
 *
 * Yahoo! Inc. licenses this file to you under the Apache License, Version
 * 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nodus.http.server.netty;

import nodus.http.server.RequestListener;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.ChannelFutureListener;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelUpstreamHandler;
import org.jboss.netty.handler.codec.http.DefaultHttpResponse;
import org.jboss.netty.handler.codec.http.HttpChunk;
import org.jboss.netty.handler.codec.http.HttpChunkTrailer;
import org.jboss.netty.handler.codec.http.HttpHeaders;
import org.jboss.netty.handler.codec.http.HttpRequest;
import org.jboss.netty.handler.codec.http.HttpResponse;
import org.jboss.netty.handler.codec.http.HttpResponseStatus;
import org.jboss.netty.handler.codec.http.HttpVersion;
import org.jboss.netty.util.CharsetUtil;

import static org.jboss.netty.handler.codec.http.HttpHeaders.is100ContinueExpected;

/**
 * @author Bruno Fernandez-Ruiz
 */
public class NettyHttpRequestAdapter extends SimpleChannelUpstreamHandler {
    private RequestListener listener;
    private NettyServerRequest req;
    private boolean https;

    public NettyHttpRequestAdapter(RequestListener listener) {
        this.listener = listener;
    }

    public void setHttps(boolean https) {
        this.https = https;
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) throws Exception {
        if (e.getMessage() instanceof HttpRequest) {
            if (listener == null) {
                sendError(ctx, HttpResponseStatus.NOT_FOUND);
            }
            HttpRequest request = (HttpRequest) e.getMessage();
            // handle Expect header automatically
            if (is100ContinueExpected(request)) {
                HttpResponse response = new DefaultHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.CONTINUE);
                ctx.getChannel().write(response);
            }
            req = new NettyServerRequest(ctx, request);
            req.setHttps(https);
            // save body that may have arrived and is not a chunk
            if (request.getContent() != ChannelBuffers.EMPTY_BUFFER) {
                req.setContent(request.getContent());
            }
            listener.service(req, new NettyServerResponse(ctx, request));
        } else if (e.getMessage() instanceof HttpChunk) {
            HttpChunk chunk = (HttpChunk) e.getMessage();
            if (chunk.isLast()) {
                HttpChunkTrailer trailer = (HttpChunkTrailer) chunk;
                if (!trailer.getHeaders().isEmpty()) {
                    req.setTrailers(trailer.getHeaders());
                }
                req.onEnd();
            } else {
                req.onData(chunk.getContent());
            }
        } else {
            super.messageReceived(ctx, e);
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
        if (e.getCause() instanceof IllegalArgumentException) {
            // we received a non-HTTP request
            ctx.getChannel().close();
        } else {
            super.exceptionCaught(ctx, e);
        }
    }

    /**
     * Creates and writes to the channel an HttpResponse with the provided
     * status. This method adds a body which is identical to the http response
     * status message.
     *
     * @param ctx    the context of this channel handler
     * @param status the http response status we want to send in the response
     */
    public void sendError(ChannelHandlerContext ctx, HttpResponseStatus status) {
        HttpResponse response = new DefaultHttpResponse(HttpVersion.HTTP_1_1, status);
        response.setHeader(HttpHeaders.Names.CONTENT_TYPE, "text/plain; charset=UTF-8");
        response.setHeader(HttpHeaders.Names.CONNECTION, HttpHeaders.Values.CLOSE);
        ChannelBuffer content = ChannelBuffers.copiedBuffer(status.getReasonPhrase(), CharsetUtil.UTF_8);
        response.setHeader(HttpHeaders.Names.CONTENT_LENGTH, content.readableBytes());
        response.setContent(content);
        ctx.getChannel().write(response).addListener(ChannelFutureListener.CLOSE);
    }
}
