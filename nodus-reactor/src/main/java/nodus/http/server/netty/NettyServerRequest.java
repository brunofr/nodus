/*
 * Copyright (c) 2011 Yahoo! Inc.
 *
 * Yahoo! Inc. licenses this file to you under the Apache License, Version
 * 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nodus.http.server.netty;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nodus.http.server.ServerRequest;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.http.HttpRequest;

/**
 * @author Bruno Fernandez-Ruiz
 */
public class NettyServerRequest implements ServerRequest {
    private final ChannelHandlerContext ctx;
    private final HttpRequest request;
    private HashMap<String, String> headers;
    private HashMap<String, String> trailers;
    private EventListener listener;
    private ByteBuffer content;
    private boolean https;

    public NettyServerRequest(ChannelHandlerContext ctx, HttpRequest request) {
        this.ctx = ctx;
        this.request = request;
    }

    @Override
    public boolean isHttps() {
        return https;
    }

    public void setHttps(boolean https) {
        this.https = https;
    }

    @Override
    public String getMethod() {
        return request.getMethod().getName();
    }

    @Override
    public String getUrl() {
        return request.getUri();
    }

    @Override
    public void setEventListener(EventListener listener) {
        this.listener = listener;
    }

    public void onData(ChannelBuffer buffer) {
        if (listener != null) {
            listener.onData(buffer.toByteBuffer());
        }
    }

    public void onEnd() {
        if (listener != null) {
            listener.onEnd();
        }
    }

    @Override
    public Map<String, String> getHeaders() {
        if (headers == null) {
            headers = new HashMap<String, String>();
            for (Map.Entry<String, String> header : request.getHeaders()) {
                headers.put(header.getKey(), header.getValue());
            }
        }
        return headers;
    }

    @Override
    public Map<String, String> getTrailers() {
        return trailers;
    }

    public void setTrailers(List<Map.Entry<String, String>> trailers) {
        this.trailers = new HashMap<String, String>();
        for (Map.Entry<String, String> trailer : trailers) {
            this.trailers.put(trailer.getKey(), trailer.getValue());
        }
    }

    @Override
    public String getHttpVersion() {
        return request.getProtocolVersion().getText();
    }

    @Override
    public ByteBuffer getContent() {
        return content;
    }

    public void setContent(ChannelBuffer content) {
        this.content = content.toByteBuffer();
    }

    public ChannelHandlerContext getContext() {
        return ctx;
    }
}
