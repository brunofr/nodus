/*
 * Copyright (c) 2011 Yahoo! Inc.
 *
 * Yahoo! Inc. licenses this file to you under the Apache License, Version
 * 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nodus.http.server.netty;

import javax.net.ssl.SSLEngine;
import java.net.InetSocketAddress;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import nodus.http.server.AbstractServer;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFactory;
import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import org.jboss.netty.handler.codec.http.HttpRequestDecoder;
import org.jboss.netty.handler.codec.http.HttpResponseEncoder;
import org.jboss.netty.handler.ssl.SslHandler;

/**
 * @author Bruno Fernandez-Ruiz
 */
public class NettyServer extends AbstractServer {
    private ServerBootstrap serverBootstrap;
    private Channel channel;

    public NettyServer() {
        init(null);
    }

    private void init(ThreadFactory threadFactory) {
        Executor bossExecutor;
        Executor workerExecutor;
        if (threadFactory == null) {
            bossExecutor = Executors.newCachedThreadPool();
            workerExecutor = Executors.newCachedThreadPool();
        } else {
            bossExecutor = Executors.newCachedThreadPool(threadFactory);
            workerExecutor = Executors.newCachedThreadPool(threadFactory);
        }
        ChannelFactory serverChannelFactory = new NioServerSocketChannelFactory(bossExecutor, workerExecutor);
        serverBootstrap = new ServerBootstrap(serverChannelFactory);
        serverBootstrap.setOption("tcpNoDelay", true);
    }

    @Override
    public void setThreadFactory(ThreadFactory threadFactory) {
        init(threadFactory);
    }

    protected void listen(InetSocketAddress address) {
        serverBootstrap.setPipelineFactory(new ChannelPipelineFactory() {
            @Override
            public ChannelPipeline getPipeline() throws Exception {
                ChannelPipeline pipeline = Channels.pipeline();
                if (credentials != null) {
                    SSLEngine engine = credentials.getSSLContext().createSSLEngine();
                    pipeline.addLast("ssl", new SslHandler(engine));
                }
                pipeline.addLast("decoder", new HttpRequestDecoder());
                pipeline.addLast("encoder", new HttpResponseEncoder());
                NettyHttpRequestAdapter adapter = new NettyHttpRequestAdapter(listener);
                adapter.setHttps(credentials != null);
                pipeline.addLast("handler", adapter);

                return pipeline;
            }
        });
        channel = serverBootstrap.bind(address);
    }

    @Override
    public void close() {
        if (channel != null) {
            channel.close();
        }
    }
}
