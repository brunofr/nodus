/*
 * Copyright (c) 2011 Yahoo! Inc.
 *
 * Yahoo! Inc. licenses this file to you under the Apache License, Version
 * 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nodus.http.server.netty;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Map;

import nodus.http.server.ServerResponse;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.http.DefaultHttpChunk;
import org.jboss.netty.handler.codec.http.DefaultHttpResponse;
import org.jboss.netty.handler.codec.http.HttpHeaders;
import org.jboss.netty.handler.codec.http.HttpRequest;
import org.jboss.netty.handler.codec.http.HttpResponse;
import org.jboss.netty.handler.codec.http.HttpResponseStatus;
import org.jboss.netty.handler.codec.http.HttpVersion;
import org.jboss.netty.util.CharsetUtil;

/**
 * @author Bruno Fernandez-Ruiz
 */
public class NettyServerResponse implements ServerResponse {
    private static String END_LINE = "";
    private HttpResponseStatus responseStatus;
    private ChannelHandlerContext ctx;
    private boolean chunked;
    private HttpResponse response;
    private ChannelBuffer content;
    private HttpRequest request;

    public NettyServerResponse(ChannelHandlerContext ctx, HttpRequest request) {
        this.ctx = ctx;
        this.request = request;
    }

    @Override
    public void writeHead(int statusCode) {
        writeHead(statusCode, null, null);
    }

    @Override
    public void writeHead(int statusCode, String reasonPhrase) {
        writeHead(statusCode, reasonPhrase, null);
    }

    @Override
    public void writeHead(int statusCode, Map<String, String> headers) {
        writeHead(statusCode, null, headers);
    }

    @Override
    public void writeHead(int statusCode, String reasonPhrase, Map<String, String> headers) {
        if (reasonPhrase != null) {
            responseStatus = new HttpResponseStatus(statusCode, reasonPhrase);
        } else {
            responseStatus = HttpResponseStatus.valueOf(statusCode);
        }
        response = new DefaultHttpResponse(HttpVersion.HTTP_1_1, responseStatus);
        if (headers != null) {
            for (Map.Entry<String, String> header : headers.entrySet()) {
                response.addHeader(header.getKey(), header.getValue());
            }
        }
        if (HttpHeaders.Values.KEEP_ALIVE.equalsIgnoreCase(request.getHeader(HttpHeaders.Names.CONNECTION))) {
            response.setHeader(HttpHeaders.Names.CONNECTION, HttpHeaders.Values.KEEP_ALIVE);
        }
        if (statusCode >= 200) {
            if (response.getHeader(HttpHeaders.Names.CONTENT_LENGTH) == null) {
                response.addHeader(HttpHeaders.Names.TRANSFER_ENCODING, HttpHeaders.Values.CHUNKED);
            }
        }
        if (HttpHeaders.Values.CHUNKED.equalsIgnoreCase(response.getHeader(HttpHeaders.Names.TRANSFER_ENCODING))) {
            chunked = true;
        }
        if (chunked) {
            ctx.getChannel().write(response);
        } else {
            content = ChannelBuffers.buffer(1024);
        }
    }

    @Override
    public void write(String chunk) {
        write(chunk, CharsetUtil.UTF_8);
    }

    @Override
    public void write(String chunk, Charset charset) {
        write(ChannelBuffers.copiedBuffer(chunk.getBytes(charset)));
    }

    @Override
    public void write(ByteBuffer binaryChunk) {
        write(ChannelBuffers.wrappedBuffer(binaryChunk));
    }

    private void write(ChannelBuffer buffer) {
        if (chunked) {
            DefaultHttpChunk chunk = new DefaultHttpChunk(buffer);
            ctx.getChannel().write(chunk);
        } else {
            // transfer
            content.writeBytes(buffer);
        }
    }

    @Override
    public void end() {
        if (chunked) {
            write(END_LINE);
        } else {
            response.setContent(content);
            ctx.getChannel().write(response);
        }
        if (HttpHeaders.Values.CLOSE.equalsIgnoreCase(response.getHeader(HttpHeaders.Names.CONNECTION)) ||
            !HttpHeaders.Values.KEEP_ALIVE.equalsIgnoreCase(request.getHeader(HttpHeaders.Names.CONNECTION))) {
            // if the client did not request keep alive, or if we issue close header
            ctx.getChannel().close();
        }
    }

    @Override
    public void end(String chunk) {
        write(chunk);
        end();
    }

    @Override
    public void end(String chunk, Charset charset) {
        write(chunk, charset);
        end();
    }

    @Override
    public void end(ByteBuffer binaryChunk) {
        write(binaryChunk);
        end();
    }
}
