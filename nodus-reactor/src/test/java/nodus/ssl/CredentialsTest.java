/*
 * Copyright (c) 2011 Yahoo! Inc.
 *
 * Yahoo! Inc. licenses this file to you under the Apache License, Version
 * 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package nodus.ssl;

import java.io.File;
import java.net.URL;

import javax.net.ssl.SSLContext;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author Bruno Fernandez-Ruiz
 */
public class CredentialsTest {
    @Test
    public void testFailConstructor() {
        try {
            new Credentials(null, null);
            fail("should have thrown exception");
        } catch (IllegalArgumentException e) {
            //
        }
    }

    @Test
    public void testFailWithPemKey() {
        URL certificate = Thread.currentThread().getContextClassLoader().getResource("server.crt");
        File fCert = new File(certificate.getFile());
        assertTrue(fCert.exists());
        URL privkey = Thread.currentThread().getContextClassLoader().getResource("server.pem");
        File fPrivkey = new File(privkey.getFile());
        assertTrue(fPrivkey.exists());
        try {
            new Credentials(fPrivkey, fCert);
            fail("should have thrown exception");
        } catch (IllegalArgumentException e) {
            //
        }
    }

    @Test
    public void testCreateWithDerKey() {
        URL certificate = Thread.currentThread().getContextClassLoader().getResource("server.crt");
        File fCert = new File(certificate.getFile());
        assertTrue(fCert.exists());
        URL privkey = Thread.currentThread().getContextClassLoader().getResource("server.der");
        File fPrivkey = new File(privkey.getFile());
        assertTrue(fPrivkey.exists());
        Credentials credentials = new Credentials(fPrivkey, fCert);
        SSLContext context = credentials.getSSLContext();
        assertNotNull(context);
        assertNotNull(context.createSSLEngine());
    }
}
