/*
 * Copyright (c) 2011 Yahoo! Inc.
 *
 * Yahoo! Inc. licenses this file to you under the Apache License, Version
 * 2.0 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package nodus.http.server.aio;

import java.io.IOException;

import org.junit.Test;

/**
 * @author Bruno Fernandez-Ruiz
 */
public class AioServerTest {
    @Test
    public void testAccept() throws IOException {
        AioServer server = new AioServer();
        server.listen(8080);
        System.in.read();
        server.close();
    }
}
